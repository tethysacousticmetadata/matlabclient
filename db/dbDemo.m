function output = dbDemo(example, varargin)
% dbDemo(exampleN, OptionalArgs)
% Examples of using the Tethys database.
% 
% example - example N, see switch statement for details.
% Optional keyword value pair arguments:
%   'Server' - Override default server name
%   'Port' - Overrride default port
%   'QueryHandler', q - Use an existing query handler rather than
%      a new one.  Note that Server and Port arguments are ignored
%      if this is specified.
%   'Debug', true|false - Produce debug information for some plots.
%
% output is only used to return values that would otherwise go out
% of scope.

% defaults
dbInitArgs = {};
debug = false;
queries = [];

output = "demo complete";  % default

idx = 1;
while idx < length(varargin)
    switch varargin{idx}
        case {'Server', 'Port'}
            dbInitArgs{end+1} = varargin{idx};
            dbInitArgs{end+1} = varargin{idx+1};
            idx = idx + 2;
        case 'QueryHandler'
            queries = varargin{idx+1};
            if ~ dbVerifyQueryHandler(queries)
                error("QueryHandler argument is not an instance " + ...
                      "of dbxml.Queries (use dbInit to create")
            end
            idx = idx + 2;
        case 'Debug'
            debug = varargin{idx+1};
            idx = idx + 2;
        otherwise
            error('Bad argument %s', char(varargin{idx}));
    end
end

% Create a handle to the query engine so that we can communicate
% with Tethys.  Typically, this should be done once per Matlab
% session as opposed to creating it every time a function is called.
% For demonstration purposes, we place it inside the demo function
% if the caller has not passed one in.
if isempty(queries)
    queries = dbInit(dbInitArgs{:});
end

% Use SIO Scripps Whale Acoustics Lab abbreviations
% for species codes as parameters.  Output representation is
% set to English.
% This must be executed after dbInit
input_abbrev = 'SIO.SWAL.v1';
dbSpeciesFmt(queries, 'Input', 'Abbrev', input_abbrev);
dbSpeciesFmt(queries, 'Output', 'Vernacular', 'English');

% Some of the example queries will restrict data to these parameters.
project = 'SOCAL';
species = 'Gg';
deployment = 38;
site = 'M';

% Examples of what can be done in Tethys:
switch example
    case 1
        % all unidentified beaked whale detections associated with a 
        % specific deployment and site in SOCAL.
        
        event(1).species = 'Zc'; %'UBW';
        event(1).call = '';  % only necessary for generating legend
        event(1).timestamps = dbGetDetections(queries, ...
            'SpeciesId', event(1).species, 'Site', site, 'Project', project, ...
            'Deployment/DeploymentNumber', deployment);
        event(1).effort = dbGetEffort(queries, ...
            'SpeciesId', event(1).species, 'Site', site, 'Project', project, ...
            'Deployment/DeploymentNumber', deployment);
        event(1).resolution_m = 60;  % plot resolution

        event(2).species = 'Anthro';
        event(2).call = 'Active Sonar';
        event(2).timestamps = dbGetDetections(queries, ...
            'SpeciesId', event(2).species, 'Site', site, 'Project', project, ...
            'Call', event(2).call, 'Deployment/DeploymentNumber', deployment);
       event(2).effort = dbGetEffort(queries, ...
            'SpeciesId', event(2).species, 'Site', site, 'Project', project, ...
            'Call', event(2).call, 'Deployment/DeploymentNumber', deployment);
        event(2).resolution_m = 60;
        
		
        event(3).species = 'Anthro';
        event(3).call = 'Ship';
        event(3).timestamps = dbGetDetections(queries, ...
            'SpeciesId', event(2).species, 'Site', site, 'Project', project, ...
            'Deployment/DeploymentNumber', deployment);
        % Get effort.  As there are multiple call types, multiple effort
        % tuples will be returned.
        event(3).effort = dbGetEffort(queries, ...
            'SpeciesId', event(3).species, 'Site', site, 'Project', project, ...
            'Deployment/DeploymentNumber', deployment);

        event(3).resolution_m = 60;
        
        % Display the plot.  The following function is part of
        % the dbDemo and is not generally available.  The functions
        % it calls are.
        plot_presence(event, debug);
        title(sprintf('%s %d site: %s', project, deployment, site));
        1;
        
    case 2
        % Humpbacks, Fin, and Blue whale calls
        % We only look for binned detections with 60 min bins.
        % On the demonstration database, there may not be effort for
        % humpbacks.
        event(1).species = 'Mn';  % humpback
        event(2).species = 'Bp';  % fin
        event(3).species = 'Bm';  % blue
        for k =1:3
          % We won't use this in the query, but when we want to add
          % a legend the code will expect a string for the call
          event(k).call = '';
          [event(k).timestamps, event(k).detections] = ...
              dbGetDetections(queries, ...
                'SpeciesId', event(k).species, 'Site', site, ...
                'Project', project, ...
                'Deployment/DeploymentNumber', deployment, ...
                'Granularity', 'binned', '@BinSize_min', 60);
          [event(k).effort, event(k).effort_details] = dbGetEffort(queries, ...
            'SpeciesId', event(k).species, 'Site', site, 'Project', project, ...
            'Deployment/DeploymentNumber', deployment, ...
            '@BinSize_min', 60);
          event(k).resolution_m = 60;  % plot resolution
        end

        plot_presence(event, debug);
        title(sprintf('%s %d site: %s', project, deployment, site));

    case 3
        % Show effort for the project, deployment, and site
        % that were set above.  Request additional information about
        % who the report Id, who submitted the detection report and 
        % what algorithm they used to be returned
        [effort, details] = dbGetEffort(queries, 'Project', project, ...
            'Deployment/DeploymentNumber', deployment, 'Site', site, 'return', ...
            ["Id", "UserId", "Algorithm"]);
        fprintf('Effort summary %s %d site %s\n', project, deployment, site);
        output = ReportEffort(details);
        
    case 4
        % Show effort for entire database
        % Warning: Will be slow on very large databases!
        [effort, details] = dbGetEffort(queries, ...
            "return", ["Id", "UserId", "Algorithm"]);
        fprintf('Effort summary across entire database\n');
        output = ReportEffort(details);
        
    case 5 
        % Diel plot in local time

        
        detections = dbGetDetections(queries, 'Project', project, ...
            'Deployment/DeploymentNumber', deployment, 'Site', site, 'SpeciesId', species);
        [effort, details] = dbGetEffort(queries, 'Project', project, ...
            'Deployment/DeploymentNumber', deployment, 'Site', site, 'SpeciesId', species);
        if ~ isempty(effort)

            % Retrieve coordinates of HARP deployment
            % (Could have been done with dbGetDetections by adding 
            % additional return data)
            sensor = dbGetDeployments(queries, 'Project', project, ...
                'DeploymentNumber', deployment, 'Site', site);
            % Determine when the sun is down between effort start and end
            EffortSpan = [min(effort(:, 1)), max(effort(:, 2))];
            % Find periods of night time.  To get complete night periods,
            % we look one day prior to the start of effort and one day
            % after.
            lat = sensor.Deployment.DeploymentDetails.Latitude{1};
            long = sensor.Deployment.DeploymentDetails.Longitude{1};
            night = dbDiel(queries, lat, long, ...
                EffortSpan(1)-1, EffortSpan(2)+1);

            % Let's plot this in local time using natical timezones
            % (does not take into account daylight savings time)
            UtcOffset = dbTimeZone(queries, lat, long, 'nautical');
            
            figure("Name", ...
                sprintf("Detections of %s in localtime with day/night", species));
            % plot diel pattern which we treat as just another detection
            % with special plotting options (no outlines, transparency,
            % & high resolution to prevent a jagged plot over time)
            visPresence(night, 'Color', 'black', ...
                'LineStyle', 'none', 'Transparency', .15, ...
                'Resolution_m', 1/60, 'DateRange', EffortSpan, ...
                'UTCOffset', UtcOffset);
            % Plot detections
            speciesH = visPresence(detections, 'Color', 'g', ...
                'Resolution_m', 5, 'Effort', effort, ...
                'UTCOffset', UtcOffset);
            legend(speciesH(1), species);
        else
            fprintf('No data for %s in %s %d Site %s\n', ...
                species, project, deployment, site);
        end
            
    case 6
        % Show all detections for a given project
        % Note that projects with duty cycling may have
        % striped patterns in their detection data.
        project = 'SOCAL';
        species = 'Bm';
        granularity = 'call';
        fprintf('All detections project %s for SpeciesId %s\n', ...
            project, species)
        dbYearly(queries, 'Project', project, 'SpeciesId', species, ...
            'Granularity', granularity);
        
    case 7
        % Provide an effort report by species, call, and effort type

        fprintf('Species having reported effort in the database\n');
        [~, details] = dbGetEffort(queries);
        if height(details.kinds_table)
            % Columns to group on
            grpcols = {'SpeciesId', 'Granularity', 'Group', 'Call'};
            % Sort in order we want to display effort
            kinds = sortrows(details.kinds_table, grpcols);
             
            % Replace any missing values so that unique plays well
            kinds.Group(ismissing(kinds.Group)) = "(none)";
            kinds.Call(ismissing(kinds.Call)) = "Any";

            % Get the starting index of each new type of effort
            [~, breaks] = unique(kinds(:, grpcols), 'stable');
            summary = kinds(breaks, grpcols);
            breaks(end+1) = height(kinds) + 1;  % last
            for group = 1:length(breaks)-1
                
                start = breaks(group);
                stop = breaks(group+1)-1;
                records = unique(kinds.RecordIdx(start:stop));
                % Find corresponding effort dates / deployments
                grpeff = details.effort_table(...
                    ismember(details.effort_table.RecordIdx, records), :);
                dur = sum(grpeff.End - grpeff.Start);
                dur.Format = 'd';  % days
                earliest = min(grpeff.Start);
                latest = max(grpeff.End);
                summary.Earliest(group) = earliest;
                summary.Latest(group) = latest;
                summary.Days(group) = dur;
                summary.EffortCount(group) = length(records);
                for datasrc = ["DeploymentId", "EnsembleId"]
                    values = grpeff(~ismissing(grpeff.(datasrc)), datasrc);
                    if ~ isempty(values)
                        summary.(datasrc +'s')(group) = strjoin(unique(values.(datasrc)), ', ');
                    end
                end
            end
        end
        % display result        
        output = summary
        
    case 8
        % Demonstrate weekly effort and diel plot for 
        % killer whale whistles.  Also shows how to retrieve
        % Latin names for species abbreviation.
        
        species = 'Oo';
        % Demonstration of finding the Latin species name to which
        % a coding corresponds.  The Integrated Taxonomic Information
        % System refers to this as the completename. 
        abbr = 'SIO.SWAL.v1';
        species_table = dbSpeciesAbbreviations(queries, abbr);
        latin = species_table.completename{strcmp(species_table.coding, species)};
        fprintf("Abbreviation %s --> %s in abbreviation map %s\n", ...
            species, latin, abbr);
            
        deploymentId = 'ALEUT02-BD';  % case sensitive
        visWeekly(queries, ...
            'DeploymentId', deploymentId,...
            'Granularity', 'encounter', 'Call', 'Whistles', ...
            'SpeciesId',species);
        title(sprintf('%s weekly effort %s site %s', ...
            latin, project, site));
                
        %And a convenience function for diel
        visDiel(queries, 'DeploymentId', deploymentId,...
            'Granularity','encounter','Call','Whistles', ...
            'SpeciesId',species, 'UTC', false);
        title(sprintf('%s diel plot %s site %s', ...
            latin, project, site));
        
    case 9
        % Lunar Illumination plot in local time

        % We wil use the Tethys 3+ style of detections and effort, 
        % taking advantage of dataframes and datetime objects
        % We ignore the first output of the dbGet... functions and
        % use the new second argument.
        [~, detections] = dbGetDetections(queries, 'Project', project, ...
             'Site', site, 'SpeciesId', species, ...
             'Deployment/DeploymentNumber', deployment);
        [~, effort] = dbGetEffort(queries, 'Project', project, ...
            'Site', site, 'SpeciesId', species, ...
            'Deployment/DeploymentNumber', deployment);
        if ~ isempty(effort)
            % Retrieve deployment associated with effort (there is only
            % one here, it is possible for there to be more than one
            sensor = dbGetDeployments(queries, ...
                'Id', effort.deployments.DeploymentId{1});
            lon = sensor.Deployment.DeploymentDetails.Longitude{1};
            lat = sensor.Deployment.DeploymentDetails.Latitude{1};
            
            % Determine when the sun is down between effort start and end
            EffortSpan = ...
                [min(effort.effort_table.Start), max(effort.effort_table.End)];
            
            % Interval minutes must evenly divide 24 hours
            interval = 30;
            getDaylight = false;
            illu = dbGetLunarIllumination(queries, lat, lon, ...
                EffortSpan(1), EffortSpan(2), interval, 'getDaylight', getDaylight);

            % We want to plot in local time, so we need to know the
            % timezone.  We'll use a nautical timezone which ignores
            % geopolitical boundaries and daylight savings time
            UTcOffset = dbTimeZone(queries, lat, lon, "nautical");
            
            night = dbDiel(queries, lat, lon, ...
                EffortSpan(1), EffortSpan(2));
            nightH = visPresence(night, 'Color', 'black', ...
                'LineStyle', 'none', 'Transparency', .15, ...
                'Resolution_m', 1/60, 'DateRange', EffortSpan, ...
                'UTCOffset', UTcOffset);
             
            lunarH = visLunarIllumination(illu, 'UTCOffset', UTcOffset);
            
            % Plot detections
            start_end = table2array(detections.detection_table(:, ["Start", "End"]));
            speciesH = visPresence(start_end, 'Color', 'b', ...
                'Resolution_m', 5, 'Effort', EffortSpan, ...
                'UTCOffset', UTcOffset);
            
            % removed no effort legend due to Matlab plot bug
            legendH = legend(speciesH(1), species);
        else
            fprintf('No effort for %s in %s %d Site %s\n', ...
                species, project, deployment, site);
        end
  
    case 10
        % Show all of the detection efforts that have been 
        % entered into the database
        % This is an example of a query written in XQuery
        fprintf('List of detection effort documents in database\n');
        % Return a Java string
        result = queries.QueryTethys(...
            strjoin([
            "<Documents xmlns=""http://tethys.sdsu.edu/schema/1.0""> {",
             "for $i in collection(""Detections"")/Detections ", 
             " return <Document> {base-uri($i)} </Document>",
             "} </Documents>"], newline));
        % indent nicely and display
        fprintf("%s\n", queries.xmlpp(result));

         
    case 11
        % Grab chlorophyll and plot next to Blue whale presence h/day
        species = 'Bm';
        deployment = dbGetDeployments(queries, 'Project', project, 'Site', site);
        [eff, eff_info] = dbGetEffort(queries, 'Project', project, 'Site', site, 'SpeciesId', species, 'Call', 'D');
        detections = dbGetDetections(queries, 'Project', project, 'Site', site, 'SpeciesId', species, 'Call', 'D');
        
        % We will be working with the 3 day chlorophyll average from
        % erdMBchla3day.  
        resolution_deg = .025;  % spatial resolution in degrees
        start = min(eff(:,1));
        stop = max(eff(:,2));
        daterange = sprintf('(%s):1:(%s)', dbSerialDateToISO8601(start), dbSerialDateToISO8601(stop));
        
        % Get mean deployment location of deployments at site (or just
        % use first one if you prefer).
        % We could loop through the deployments and build up the list of
        % longitudes and latitudes, but we will use an anonymous function
        % that is applied to each element of the deployment array.  The 
        % function just accesses the lat/long for one deployment:
        %   @(x) x.Deployment.DeploymentDetails.Latitude{1}
        % We wrap this inside an arrayfun which applies the function
        % to each of the deployments.  It is equivalent to:
        % function lats = Latitude(deployment)
        %   lats = zeros(length(deployment), 1);
        %   for idx=1:length(lats)
        %       lats(idx) = deployment(idx).Deployment.DeploymentDetails.Latitude{1}
        %   end
        lat = arrayfun(@(x) x.DeploymentDetails.Latitude{1}, deployment.Deployment);
        long = arrayfun(@(x) x.DeploymentDetails.Longitude{1}, deployment.Deployment);
        % Take average and round to resolutoin
        lat = round(mean(lat)/resolution_deg)*resolution_deg;
        long = round(mean(long)/resolution_deg)*resolution_deg;
        
        % Specify the erddap server.  An optional second argument specifies
        % the server.  If not provided, a default server will be used
        upwell = dbERDDAPServer(queries, "https://upwell.pfeg.noaa.gov/erddap");
        % 3-day composite of chlorophyll-A from NASA Aqua (experimental)
        dataset = 'erdMBchla3day'; 
        measurement = 'chlorophyll';

        fprintf('Running ERDDAP query on node %s.\n', upwell.getServer())
        fprintf('Query time depends on size of data set and ERDAP server \n');
        fprintf('load. Tethys remembers query results for a while, so repeat\n');
        fprintf('queries for the same data are typically faster\n\n');
        fprintf("The %s is one that is typically very ", dataset);
        fprintf("slow (10's of min).\n");
        fprintf("The ERDDAP server sometimes returns errors when serving it.\n");
        
        timer = tic;
        result = upwell.get(sprintf('%s?%s[%s][(0.0):1:(0.0)][(%f):1:(%f)][(%f):1:(%f)]', ...
            dataset, measurement, daterange, lat, lat, long, long));
        fprintf('ERDAP query completed, %2.2f min\n', toc(timer)/60.0);
        [counts, days] = dbPresenceAbsence(detections(:,1));
        weeks = days(1:90:end);      
        dcallH = plot(days, sum(counts,2));
        callAx = gca;
        %turn off upper/right tick marks
        set(gca,'box','off');

        % Find the parent of the peer axis so that we may put the new axis
        % in the same container.
        parent = get(callAx, 'Parent');


        % Position a second axis on top of our current one.
        % Make it transparent so that we can see what's underneath
        % turn on the tick marks on the X axis and set up an alternative
        % Y axis in another color on the right side.
        envAx = axes('Position', get(callAx, 'Position'),  ... % put over
            'Color', 'none', ... % don't fill it in
            'YAxisLocation', 'right', ...  % Y label/axis location
            'XTick', [], ...  % No labels on x axis
            'YColor', 'm', ...
            'Parent', parent);
        hold(envAx);
        
        % access time axis values.
        % find axis named time in list of axes
        timeaxpos = find(strcmp(result.Axes.names, 'time'));
        
        plot(envAx, ...
            result.Axes.values{timeaxpos}, result.Data.values{1}(:), 'm');
        
        set(envAx, 'XTick', []);
        set(callAx, 'XTick', weeks);
        ylabel(envAx, 'Chlorophyll mg/m^{3}')
        ylabel(callAx, 'Num. Hours w/ D-Calls / day');
        datetick(callAx, 'x', 1, 'keeplimits', 'keepticks');
        set(envAx, 'YLim', [0, 7])
                
    case 12
        % ERDDAP demonstration
        
        % Specify the erddap server.  An optional second argument specifies
        % the server.  If not provided, a default server will be used
        upwell = dbERDDAPServer(queries, "https://upwell.pfeg.noaa.gov/erddap");

        % longitude & latitude bounding box around an instrument
        % deployed on the south eastern bank of the Santa Cruz Basin
        % in the Southern California Bight
        
        % Normally, we would query for a specific instrument, but
        % here we just hardcode the site
        center = [33.515  240.753];  % close to site M
        start = '2010-07-22T00:00:00Z';
        stop = '2010-11-07T08:49:59Z';
        
        % Here is how we would do it for a generic instrument:
        % deployment = dbGetDeployments(query_eng, ... details...);
        % Assume that only a single deployment was matched, otherwise
        % we have more work to do
        % center = [deployment.DeploymentDetails.Longitude, ...
        %           deployment.DeploymentDetails.Latitude];
        % start = deployment.DeploymentDetails.TimeStamp;
        % stop = deployment.DeploymentDetails.TimeStamp;        
        
        % Find a bounding box about 5 km away from our center.
        distkm = 5;
        deltadeg = km2deg(distkm);  % Requires Mapping toolbox, about .045 degrees
        box = [center-deltadeg; center+deltadeg];
        
        % Find a list of sea surface tempature datasets within bounding box
        criteria = ['keywords=sea_surface_temperature', ...
            sprintf('&minLat=%f&maxLat=%f&minLong=%f&maxLong=%f', box(:)), ...
            sprintf('&minTime=%s&maxTime=%s', start, stop)];
        fprintf("Opening a browser window with data sets covering sea\n");
        fprintf("surface temperature in our region & time of interest.\n");
        x = input('Press Enter to open window');
        datasets = upwell.search(criteria);
        
        fprintf('\nAt this point, we would select a dataset.\n');
        fprintf('For this demo, we are selecting the 8 day \n')
        fprintf('sea surface temperature composite in erdMWsstd8day\n');
        
        x = input('Press Enter to continue');

        % Format the ERDDAP query
        % We use our start/stop times, and the coordinates created using
        % our bounding box.
        erd_str = ['erdMWsstd8day?sst',...
            sprintf('[(%s):1:(%s)][(0.0):1:(0.0)]',start,stop),...
            sprintf('[(%f):1:(%f)][(%f):1:(%f)]',box(:))];
        
        fprintf('Retrieving sea surface temperature with function call:\n');
        fprintf('data = dbERDDAP(queries,%s)\n',erd_str);
        data = upwell.get(erd_str);
        
        fprintf('The result is a structure containing three fields:\n');
        disp(data);
        
        x = input('Press Enter to continue');
        fprintf('The Axes field is a structure describing each of the 4 axes:\n');
        disp(data.Axes);
        fprintf('Names is the name of each axis, e.g. data.Axes.names(1) is longitude\n');
        fprintf('Units are the measurements, types are the data types, and \n');
        fprintf('values is the actual point on the axis\n\n');
        
        fprintf('the Data field is a struct with similar child fields:\n')
        disp(data.Data)
        fprintf('the values field of Data represents the coordinate values for each dimension\n\n');
        
        
        fprintf("Now let us show an animation of latitude, longitude and sea surface temperature\n");
        fprintf('Note that some days may be missing data, they are displayed in white\n');
        x = input('Press Enter to continue');
        
        sst = data.Data.values{1};  % sea-surface temperature 
        
        % Find the limits of the sea surface temperature
        % To find min/max of a tensor, use (:) to create vector
        targetlims = [min(sst(:)), max(sst(:))];
        
        %replace NaNs (missing values)
        % with dummy value so that they can be a different color
        
        maxval = targetlims(2);
        sst(isnan(sst)) = maxval+maxval/10;
        
        timeax = find(strcmp(data.Axes.names, 'time'));
        h = figure('Name', 'Sea Surface Temperature (SST)','Visible','off');
        
        for tidx=1:data.dims(timeax);  % Animate over time
            % display one time slice, squeeze extracts the single
            % time slice as a matrix.
            imagesc(data.Axes.values{1}, data.Axes.values{2}, ...
                squeeze(sst(:,:,tidx)), targetlims);
            set(gca, 'YDir', 'normal');
            title(datestr(data.Axes.values{timeax}(tidx), 1));
            xlabel(data.Axes.names{1});
            ylabel(data.Axes.names{2});
            
            %a trick to make NaNs gray
            colordata = colormap;
            colordata(end,:) = [0.9 0.9 0.9];
            colormap(colordata);
            cbh = colorbar;
            ylabel(cbh, 'sst (deg C)')
            frames(tidx) = getframe(h);
        end
        close(h);
        
        %new figure for the movie
        figure('Name','Sea Surface Temperature (SST)');
        movie(gcf,frames,1,8)

    case 13
        fprintf('ERDDAP Wind speed visualization across Channel Island\n')
        fprintf('National Marine Sactuary HARP deployments\n');
        % Second ERDDAP demonstration, more sophisticated
        % Find wind coverage across all Channel Islands project
        % HARPS (assumes demo database is running)
        project = 'CINMS';
        deployments = dbGetDeployments(queries, 'Project', project);
        % Find temporal extent
        % deployments is a structure array.  We can get back a cell
        % array of DeploymentDetails by using
        % deployments.DeploymentDetails.  However, we want the timestamp
        % associated with each of these.  We use an anynonymous function
        % @(x) x.TimeStamp which just takes its input and returns the
        % timestamp and apply it to each cell of
        % {deployments.DeploymentsDetails}.  The UniformOutput flag
        % tells us that the results should be treated as a cell array.
        %
        %Data collection for this set ended 2011-12-27, so queries
        %exceeding this date will give an error.
        
        % Extract an array structure
        dep_array = [deployments.Deployment];  
        
        breaks = arrayfun(@(x) x.DeploymentDetails.TimeStamp{1}, dep_array);
        
        % string timestamp with earliest deployment
        earliest = dbSerialDateToISO8601(min(breaks));  
        % For this demonstration, we will pad a year to the effort end date.
        % We could have used 
        latest = dbSerialDateToISO8601(addtodate(min(breaks),1,'year'));
        
        % We could take the max of the end times, but ERDDAP can be
        % a bit slow when requests are large.
        
        % Similar techniques to get the long/lat bounding box
        latitudes = arrayfun(@(x) x.DeploymentDetails.Latitude{1}, dep_array);
        longitudes = arrayfun(@(x) x.DeploymentDetails.Longitude{1}, dep_array);
        latspan = minmax(latitudes);
        lonspan = minmax(longitudes);
        
        % Specify the erddap server.  An optional second argument specifies
        % the server.  If not provided, a default server will be used
        upwell = dbERDDAPServer(queries, "https://upwell.pfeg.noaa.gov/erddap");

        % Build the query
        % ST, Pathfinder Ver 5.2 (L3C), Day and Night, Global, 0.0417°, 
        % 1981-2012, Science Quality (1 Day Composite)
        % See ERDDAP for details on sensors, time coverage, etc.
        % https://upwell.pfeg.noaa.gov/erddap/griddap/erdPH2ssta1day.html
        dataset = 'erdPH2ssta1day';         
        target = 'wind_speed';  % variable we wish to retrieve
        % Axes for this grid are [time][latitude][longitude]
        % latitude/longitude .0417° 
        timeaxis = sprintf('[(%s):1:(%s)]', earliest, latest);
        lataxis = sprintf('[(%f):1:(%f)]', latspan);
        % These data use -180 to 180 degrees East (data sets vary)
        % As Tethys stores positive degrees East, we need to convert
        lonspan(lonspan > 180) = lonspan(lonspan > 180) - 360;
        longaxis = sprintf('[(%f):1:(%f)]', lonspan);
        querystr = sprintf('%s?%s%s%s%s', ...
            dataset, target, timeaxis, lataxis, longaxis);
        % Run the query
        % Returns a structure with:
        %  Axes - information about axes
        %  Data - information about the data and the values
        %  dims - data dimensions
        % NOTE:  Axes of returned data are in reverse order
        result = upwell.get(querystr);
        
        % Let's animate the windspeed
        
        % Find how the data was organized
        timeax = find(strcmp(result.Axes.names, 'time'));
        longax = find(strcmp(result.Axes.names, 'longitude'));
        latax = find(strcmp(result.Axes.names, 'latitude'));
        % We only asked for one variable, so we know it's in axis 1
        % but this is how we would check generally
        windax = find(strcmp(result.Data.names, "wind_speed"));
        wind = result.Data.values{windax};
        
        % code relies on time axis being the 3rd dimension
        % we won't try to generalize the code, but will throw
        % an error if this is not the case
        assert(timeax == 3, 'Time is not the third axis, cannot run')
        
        % Find the limits of the wind speed
        targetlims = [min(wind(:)), max(wind(:))];
        
        % Set missing data to something 10% above largest
        wind(isnan(wind)) = targetlims(2) * 1.1;

        %don't need to display it until the animation is created.
        %otherwise it will be shown twice.
        h = figure('Name', target,'Visible','off');
        
        fprintf('Creating animation...\n');
        for tidx=1:result.dims(timeax);  % Animate over time
            imagesc(result.Axes.values{1}, result.Axes.values{2}, ...
                squeeze(result.Data.values{1}(:,:,tidx)), targetlims);
            set(gca, 'YDir', 'normal');
            title(datestr(result.Axes.values{timeax}(tidx), 1));
            xlabel(result.Axes.names{1});
            ylabel(result.Axes.names{2});
            
            %a trick to make NaNs gray
            colordata = colormap;
            colordata(end,:) = [.9 .9 .9];
            colormap(colordata);
            cbh = colorbar;
            ylabel(cbh, 'wind speed m/s')
            frames(tidx) = getframe(h);
        end
        close(h)
        if true
            %new figure for the movie
            figure('Name', target);
            %1 time, 6FPS
            movie(gcf,frames,1,8)
        else
            % Write the movie to a file
            % For axes to display correctly, getframe above 
            % should take the argument of the figure handle
            % getframe(h).  Note that this is incompatible with movie
            % so we don't have this set by default.
            video = VideoWriter(dataset);
            video.FrameRate = 4;  % frames/s
            open(video);
            for idx = 1:length(frames)
                writeVideo(video, frames(idx));
            end
            close(video);
        end
        
    otherwise
        error('Unknown example');
end
       
function plot_presence(event, debug)
% plot_presence
% Show the presence/absence plot over multiple results
% event - Structure array with fields
%    effort - Nx2 effort array
%    timestamps - Nx1 or Nx2 set of detection times
%    resolution_m - Size of presence bins in min
%    species - category label
%    call - call type
count = 1;
N = length(event);
figure('Name', ['Presence/absence ', sprintf('%s, ', event.species)]);
colors = cool(N);
% Which queries had effort associated with them?
EffortPred = arrayfun(@(x) ~isempty(x.effort), event);
EffortN = sum(EffortPred);
NoEffortN = N - EffortN;
event_H = cell(EffortN, 1);
event_label = cell(EffortN, 1);
if NoEffortN
    fprintf('Skipping due to lack of effort -----\n');
    for idx = find(EffortPred == 0)
        fprintf('%s %s\n', event(idx).species, event(idx).call);
    end
    fprintf('-------------\n')
end
for idx = find(EffortPred)
    % plot the detections.  Returns handles to patch objects for 
    % detections (1) and effort (2)
    event_H{count} = visPresence(event(idx).timestamps, ...
        'Effort', event(idx).effort, ...
        'Resolution_m', event(idx).resolution_m, 'Color', colors(idx,:), ...
        'BarHeight', 1/EffortN, 'BarOffset', (EffortN-count)/EffortN, 'Debug', debug);
    event_label{count} = sprintf('%s %s', event(idx).species, event(idx).call);
    count = count + 1;
end
% Concatenate all the handles so that we have a matrix where
% row 1 shows detections and row 2 shows effort 
event_handles = cat(1, event_H{:});
event_handles = reshape(event_handles, 2, length(event_handles)/2);

% if no event occurred, then use effort handle
use_effort = event_handles(1,:) == 0;
use_handles = event_handles(1,:);
use_handles(use_effort) = event_handles(2,use_effort);

legend(use_handles, event_label);

% If you want your plots to have the most recent date at the bottom,
% uncomment the next line:
% set(gca, 'YDir', 'reverse');

    
function effort_kind = ReportEffort(details)
    % ReportEffort - Generate a large table
    % Merge the kinds and effort tables
    effort_kind= join(details.kinds_table, details.effort_table, ...
        "Keys","RecordIdx", "KeepOneCopy","RecordIdx");




