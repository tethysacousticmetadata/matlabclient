classdef dbERDDAPServer < handle

    % Class to handle ERDDAP requests
    %
    % Create an instance of the class, specifying:
    %   the Tethys query handler to be used
    %   Optional ERDDAP URL including the path to ERDDAP.  Examples
    %     https://coastwatch.pfeg.noaa.gov/erddap
    %     https://www.ncei.noaa.gov/erddap
    %     https://erddap.dataexplorer.oceanobservatories.org/erddap
    %     
    %     If ERDDAP server is not specified, the server will use a
    %     default ERDDAP server, use getServer to determine which one.
    %
    % Instances suppor the following interface methods, 
    % see method documentation for details:
    % get - retrieves ERDDAP data
    % search - search for ERDDAP data
    % getServer - Report which ERDDAP server is being used

    properties
        TethysHandler
        default_server  % Using ERDDAP default_server
        erddap_server  % ERDDAP external collection
    end

    methods (Access = public)

        function obj = dbERDDAPServer(TethysQueryHandler, ERDDAPServer)
            % dbERDDAPServer(QueryHandler, ERDDAPServer)
            % Create an object that can query the specified ERDDAP server
            % through the specified TethysQueryHandler.
            %
            % TethysQueryHandler - Java object created by dbInit
            % ERDDAPServer - Name of an ERDDAP server that will be
            %   contacte using secure http protocol.  If not provided
            %   defaults to the ERDDAP server defined by the Tethys
            %   server.  Server may either be a complete URL to the ERDDAP
            %   resource or a machine name.  Examples:
            %       https://coastwatch.pfeg.noaa.gov/erddap
            %       coastwatrch.pfeg.noaa.gov
            obj.TethysHandler = TethysQueryHandler;
            if nargin > 1
                % User specified a specific ERDDAP server
                resource = obj.server_to_resource(ERDDAPServer);
                obj.erddap_server = resource;
                obj.default_server = false;
            else
                obj.default_server = true;
                obj.erddap_server = "";  

            end
        end

        function result = get(obj, Query, squeezeP)
            % result = erddap.get(Query, squeezeP)
            % Run an ERDDAP query
            % Querry - ERDDAP query specifciation
            % squeezeP - Collapse singleton dimensions in grid data
            %    (default false)
            %
            % Returns data as table or grid depending on the data set
            % queried.
            %
            % Query is a string that specifies the ERDDAP data to retrieve and is
            % data set dependent.   As an example, the 1-day composite for sea
            % surface temperature, erdGAssta1day (see
            % https://coastwatch.pfeg.noaa.gov/erddap/griddap/erdGAssta1day.html)
            % is a grid with 4 dimensions:
            %   time, altitude, latitude, longitude
            % When specifying the search, the Query string has the format:
            %   data_set_name?variable[span1][span2][...][spanN]
            % where
            %   variable is the name of the variable that we wish to return
            %   span_i is a data range over each of the variables.
            %       All spans have three colon separate values:
            %              [start:step:end]
            %
            %       Span starts and ends are indices into the grid, but this is
            %       rarely convenient.  Starts and ends can be specified in the
            %       unit of the data set axes by placing values in ( ), e.g.:
            %           sst observations between July 24 2009 and September 16, 2009
            %           sst[(2009-07-24T00:00:00Z):1:(2009-09-16T00:00:00Z)]
            %       Note that the units must be in the same format as ERDDAP.
            %       Note that both Tethys and ERDDAP use ISO8601 for time
            %       YYYY-MM-DDTHH:MM:SSZ where Z means UTC (function
            %       dbSerialToISO8601 will convert Matlab serial date numbers to
            %       ISO8601).  Longitude is always expressed in degrees east, but
            %       read the documentation for the data set to determine the interval.
            %       It may be in the range [-179, 180) in which case you will need
            %       to convert longitudes that are stored in Tethys's [0, 360).
            %
            %       Sometimes, a variable only has a single value, such as elevation in
            %       a sea surface temperature plot.  These axes can be represented as
            %       a span with the same start/end value: [(0.0):1:(0.0)] or [0:1:0].
            %
            % A complete query string for the sea surface temperature data might
            % look like the following:
            % 'erdGAssta1day?sst[(2009-07-24T00:00:00Z):1:(2009-09-16T00:00:00Z)][(0.0):1:(0.0)][(32.559):1:(32.759)][(240.423):1:(240.623)]'
            %
            % result is a structure with the following structure:
            %
            % For grids ------------------------------------------------------------
            %   dims - Vector of grid dimensions
            %   Axes - Structure with information about the axes, fields:
            %       names - Cell array with axis names
            %       units - Cell array of units associated with type
            %       types - Cell array of data type of axis values:
            %               datenum, String, numeric type
            %               Note that numeric types are stored as doubles in Matlab,
            %               but their original precision (e.g. float, double, int)
            %               can be determined from this field.
            %       values - Cell array of grid axis labels
            %   Data - Structure with grid data
            %       names - Cell array with data variable names
            %       units - Cell array with data units
            %       types - Cell array of data types:
            %               datenum, String, double
            %       values - Cell array of data values.
            %           Each cell entry is one variable
            %
            % Grid example:
            % q = dbInit();  % update for  your server
            % erddap = dbERDDAPServer(q);
            % r = erddap.get("erdGAssta1day?sst[(2009-07-24T00:00:00Z):1:(2009-09-16T00:00:00Z)][(0.0):1:(0.0)][(32.559):1:(32.759)][(240.423):1:(240.623)]");
            %
            % r.Axes.names:  'time'    'altitude'    'latitude'    'longitude'
            % r.Axes.units:  'UTC'    'm'    'degrees_north'    'degrees_east'
            % r.Axes.types:  'datenum'    'double'    'double'    'double'
            % r.Axes.values{1} contains datenums indicating the sampling points on the
            %   time axis, r.Axes.units{3} contains latitudes, etc.
            % r.Data.names:  'sst'
            % r.Data.units:  'degree_C'
            % r.Data.types:  'float'
            % r.Data.values{1} contains sea surface temperature (SST) measurements
            %
            % If mulitple varaibles were requested (not possible with this specific
            % dataset), then r.Data.values would contain additional cells.
            %
            % Removing singleton axes (squeeze predicate)
            % Note that the altitude is constant as the altitude of the sea surface
            % is always zero, making for r.Data.values matrices that have a only
            % one value along the altitude axis.  (In this example, a 55 x 1 x 5 x 5
            % matrix).  To remove the singleton axis, set the optional squeeze predicate
            % (squeezeP) to true.
            %
            % The query format
            % Squeeze example:
            % erddap = dbERDDAPServer(q)  % q set earlier by dbInit
            % r = erddap.get('erdGAssta1day?sst[(2009-07-24T00:00:00Z):1:(2009-09-16T00:00:00Z)][(0.0):1:(0.0)][(32.559):1:(32.759)][(240.423):1:(240.623)]', true);
            %
            % Ouput will be similar, the Axes and Data fields will have the same
            % structure except elements that contain only a single value will be
            % removed.  Hence, in this example, altitude will be removed and
            % results.Data.values{1} will be 55 x 5 x 5 instead of 55 x 1 x 5 x 5.
            %
            % A new constants field shows the singleton axes that were "squeezed" out.
            % r.Constants:
            %     names: {'altitude'}
            %     units: {'m'}
            %     types: {'double'}
            %    values: {[0]}
            %
            % for Tables ------------------------------------------------------------
            %   rows - Number of rows in table
            %   Columns - Structure containing information about each table
            %       names - Cell array of column names
            %       types - Cell array of column types
            %               datenum, String, numeric type
            %               Note that numeric types are stored as doubles in Matlab,
            %               but their original precision (e.g. float, double, int)
            %               can be determined from this field.
            %       units - Cell array of units of measure if applicable ([] if not)
            %   Data
            %       fields corresponding to the names.  Each field is a cell array
            %       or vector depending upon its data type.
            %
            % Example:
            % r = dbERDDAP(q, 'erdCalcofiBio?line_station,line,station,longitude,latitude,depth,time,occupy,obsCommon,obsScientific,obsValue,obsUnits&time>=2004-11-12T00:00:00Z&time<=2004-11-19T08:32:00Z');
            % r
            %       Columns: [1x1 struct]
            %       Data: [1x1 struct]
            %       rows: 296
            % r.Columns.names'
            %    'line_station' 'line' 'station' 'longitude' 'latitude' 'depth'
            %    'time' 'occupy' 'obsCommon' 'obsScientific' 'obsValue' 'obsUnits'
            %     7
            % datestr(r.Data.time)  OR using a technique that can be applied to loops
            %       fieldname = 'time';
            %       datestr(r.Data.(fieldname))  % .(variable) use contents as name
            % returns
            %   18-Nov-2004 11:57:00
            %   18-Nov-2004 11:57:00
            %   18-Nov-2004 11:57:00
            %   ...

            debug = false;

            xQuery = sprintf('%s/%s!', obj.getCollection("erddap"), Query);
            dom = obj.TethysHandler.QueryReturnDoc(xQuery);

            if nargin < 3
                squeezeP = false;
            end

            % Determine data type
            types = {
                'Table', @(x) dbERDDAPServer.dbGetTableDap(x)
                'Grid', @(x) dbERDDAPServer.dbGetGridDap(x, squeezeP)
                };
            found = false;
            idx = 1;
            while ~ found && idx <= size(types, 1)
                node = dbXPathDomQuery(dom, types{idx, 1});
                found = node.getLength() == 1;
                if found
                    result = types{idx, 2}(dom);  % process
                else
                    idx = idx + 1;
                end
            end

            if ~ found || debug
                text = dbDom2XML(dom);
                if debug
                    debugH = fopen('queryresults.xml', 'w');
                    fwrite(debugH, xQuery);
                    fwrite(debugH, '\n');
                    fwrite(debugH, char(obj.TethysHandler.xmlpp(text)));
                    fwrite(debugH, '\n');
                    fclose(debugH);
                end
                if ~ found
                    error('Unable to parse\n%s', text);
                end
            end
        end

        function url = search(obj, SearchParams, OpenBrowser)
            % url = search(SearchParams, OpenBrowser)
            % Search NOAA's Environmental Research Division Data Access Program
            % (ERDDAP) catalog for datasets matching desired parameters.
            % SearchParams arguments are any valid set of ERDDAP keywords.  Each
            % keyword is followed by an = sign with a search value.  Multiple keywords
            % are joined by &.
            %
            % If the optional OpenBrowser argument (default true) is true, a web browser
            % will display the search results.  The return value url is the url that
            % is returned.
            %
            % ERDDAP's web services discussion gives a couple of examples and contains
            % a pointer to a GUI which will let people observe all settable parameters:
            % http://coastwatch.pfeg.noaa.gov/erddap/rest.html
            %
            % Search parameters as of this writing:
            %         searchFor - search terms separated by +, e.g. night+modis
            %         protocol
            %         cdm_data_type
            %         institution
            %         ioos_category
            %         long_name
            %         standard_name
            %         minLat  - Latitude is in degrees North
            %         maxLat
            %         minLon  - Longitude is in degrees East
            %         MaxLon
            %         minTime - Time is in the ISO 8601:2004 format
            %         maxTime   e.g. 2012-01-01T18:34:22Z
            %
            %
            % Examples:
            % q = dbInit();  % update for  your server
            % erddap = dbERDDAPServer(q);
            % 
            % dbERDDAPSearch(queryH, 'ioos_category=ice_distribution')
            %
            % dbERDDAPSearch(queries, 'keywords=sea_surface_temperature&minLat=33.47&maxLat=33.56&minLon=240.71&maxLon=240.80')


            if nargin < 3
                OpenBrowser = true;  % default if omitted
                if nargin < 2
                    SearchParams = '';
                end
            end

            dom = obj.TethysHandler.QueryReturnDoc(...
                sprintf('%s/%s!', ...
                    obj.getCollection("erddap_search"), SearchParams));

            [dontcare, url] = dbXPathDomQuery(dom, 'url');
            if iscell(url)
                url = url{1};
            end
            if OpenBrowser
                web(url, '-browser');
            end
        end

        function server = getServer(obj)
            % server = getServer(obj)
            % Report server used by this ERDDAP object
            
            if obj.default_server
                try
                    xml = obj.TethysHandler.resource("Tethys/erddap");
                catch
                    error("Unable to communicate with Tethys server");
                end
                xml = string(xml);
                tokens = regexpi(xml, ...
                        '<erddap>(.*)</erddap>', 'tokens');
                if isempty(tokens)                    
                    error("Unexpected Tethys server response " + ...
                        "to Tethys/erdap request: " + xml);
                else
                    server = tokens{1};
                end                
            else
                server = obj.erddap_server;
            end
        end

        function categories(obj)
            % obj.categories()
            % Open browser window that shows categories that can 
            % be used in an ERDDAP data set search.
            % The window has a list of attributes such as ioos_category
            % and keywords.
            % Selecting one of the attributes shows the list of permitted
            % standard names (values) that can be used with the attribute.
            %
            % These pairs can be used in an ERDDAP search, e.g., 
            % obj.search("ioos_category=ice_distribution&ioos_category=salinity")

            server = obj.getServer();
            url = server + "/categorize";
            web(url, "-browser")
        end

    end

    methods (Access = private)

        function collection = getCollection(obj, collection)
            % collection = obj.getCollection(collection)
            % Internal function to format the Tethys collection string

            if obj.default_server
                collection = sprintf('collection("ext:%s")', collection);
            else
                collection = sprintf('collection("ext:%s:server=%s")', ...
                    collection, obj.erddap_server);
            end
        end

        function resource = server_to_resource(obj, server)
            % resource = obj.server_to_resource(server)
            % Users can specify ERDDAP servers with either a resoruce
            % path, e.g. https://server/erdap or
            % via a machine name that is assuumed to be serving the
            % ERDDAP protocol using https: at the on resource /erdap.

            import java.net.URL;

            resource = [];
            if isa(server, "matlab.net.URI")
                % We assume that this is a valid URI, As of R2024a
                % Mathworks doesn't do a lot of validation (e.g.,
                % there can be an emtpy scheme (https)), but if
                % the user has crated a matlab.net.URI we'll assume
                % that they know what they are doing.
                resource = server.EncodedURI;  % pull URI string
            else
                try
                    url = URL(server);
                    uri = url.toURI();
                    % The above will fail if server is not a
                    % well-formed URL
                    resource = server;  % valid URL
                catch
                    % do nothing
                end

                if isempty(resource)
                    if ischar(server) || isstring(server)
                        resource = sprintf("https://%s/erddap", server);
                    else
                        error("Specified server is not a valid URL")
                    end
                end
            end
        end
    end

    methods (Access = private, Static=true)

        function results = dbGetGridDap(dom, squeezeP)

            domdims = dbXPathDomQuery(dom, 'Grid/Dims');
            dims = char(dbDomGetValue(domdims.item(0).getChildNodes(), 0));
            dims = sscanf(dims, '%d')';

            results.Axes = dbERDDAPServer.populate(dom, 'Axes');
            results.Data = dbERDDAPServer.populate(dom, 'Data');
            for idx = 1:length(results.Data.values)
                results.Data.values{idx} = reshape(results.Data.values{idx}, dims);
            end

            if squeezeP
                % Remove any singleton dimensions
                singletonP = dims == 1;
                if any(singletonP)
                    % Copy singletons to a constants structure and remove
                    % them from the Axes structure
                    for f = {'names', 'units', 'types', 'values'}
                        f = f{1};
                        results.Constants.(f) = results.Axes.(f)(singletonP);
                        results.Axes.(f)(singletonP) = [];
                    end
                    for idx = 1:length(results.Data.values)
                        results.Data.values{idx} = squeeze(results.Data.values{idx});
                    end
                    dims(singletonP) = [];
                end

            end
            results.dims = dims;

        end

        function result = populate(dom, element)
            % result = populate(dom, element)
            % Given a dom with an element that has the following children:
            % Names, Units, Types, and Values
            % extract the data into a structure

            path = sprintf('Grid/%s', element);
            % Pull out the names, units, and types for axes
            namesnode = dbXPathDomQuery(dom, sprintf('%s/Names/item', path));
            result.names = dbERDDAPServer.getItems(namesnode);
            unitsnode = dbXPathDomQuery(dom, sprintf('%s/Units/item', path));
            result.units = dbERDDAPServer.getItems(unitsnode);
            typesnode = dbXPathDomQuery(dom, sprintf('%s/Types/item', path));
            result.types = dbERDDAPServer.getItems(typesnode);

            % Pull out values for axes
            valuesnodes = dbXPathDomQuery(dom, sprintf('%s/Values', path));
            result.values = cell(valuesnodes.getLength(), 1);  % preallocate
            for idx=1:length(result.values)  % populate
                if strcmp(result.types{idx}, 'String')
                    values{idx} = dbERDDAPServer.getItems(valuesnodes.item(idx-1));
                    if strcmp(result.units{idx}, 'UTC')
                        result.values{idx} = dbISO8601toDatetime(values{idx});
                        result.types(idx) = 'datetime';
                    end
                else
                    result.values{idx} = sscanf(...
                        char(...
                        valuesnodes.item(idx-1).getFirstChild().getNodeValue()...
                        ), '%g');
                end
            end
        end

        function values = getItems(domarray, convert)
            % Expecting a list of nodes with the same element, e.g.
            %  <item> a </item>
            %  <item> b </item>
            % extract an array/vector of values.
            % By default treats as characters and returns a string array.
            % The optional convert allows specification of a different
            % convervsion function, e.g. @double in which case a vector is
            % returned.

            len = domarray.getLength();

            numeric = nargin > 1 && ~isequal(convert, @char);

            if numeric
                values = zeros(len, 1);
                for idx=1:len
                    values(idx) = convert(string(domarray.item(idx-1).item(0).getNodeValue()));
                end
            else
                values = strings(len, 1);
                whitespaceP = false(len, 1);
                for idx=1:len
                    item = domarray.item(idx-1);
                    textnode = item.item(0);
                    if ~ isempty(textnode)
                        values(idx) = string(textnode.getNodeValue());
                    else
                        % We should have been able to have the document parser
                        % eliminate the whitespace nodes on the Java side,
                        % unclear why this is not working.   Workaround...
                        whitespaceP(idx) = true;  % Note non-text nodes
                    end
                end
                values(whitespaceP) = [];  % Remove non-text nodes
            end
        end

        function value = dbDomGetValue(domItem, idx)
            if domItem.hasChildNodes()
                value = domItem.getFirstChild().getNodeValue();
            else
                value = [];
            end
        end
    end
end