function [timestamps, info] = dbGetEffort(queryEngine, varargin)
% [Timestamps, Data] = dbGetEffort(queryEngine, Arguments)
% Retrieve effort information from Tethys detection effort records.
% Timestamps is a matrix of Matlab serial dates containing the start and
% end times of effort in each row.  Info is a structure 
% that describes the effort.  Info.Detections contains
% information relating to each row of timestamps matrix (e.g. Kind 
% indicates lists of species the effort corresponded to and the granularity
% of the detection effort).
%
% queryEng must be a Tethys database query object, see dbDemo() for an
% example of how to create one.
%
% Arguments are used to specify selection criteria for the effort as well
% as to return additional information such as deployment locations, sample
% rate, etc.  These are processed in the same manner as dbGetDetections.
%
% Returns:
%   timestamps - Rows of Matlab serial dates for start and end effort
%   
%   info - if specified, returns a structure with fields
%     > deployments - stucture array noting the EnsembleId or DeploymentId
%        (separate fields) to which the effort corresponds.
%     > data - structure representing the data returned from the database
%     > effort_table - Matlab table of effort datetimes with start and end
%        as datetimes (easier to read/manipulate), and a RecordIndex.
%        The RecordIndex can be used to index the data and deployments
%        fields
%   
% Example on the MBARC database
% Retreive effort to detect individual A, B, and D calls produced by
% blue whales on projects associated with the Southern California Bight
% (SOCAL, CINMS)
%
% Assumes: query handler (dbInit output) is set to q and
% that we are using the SIO.SWAL.v1 species abbrevation map
% which can be set with dbSpeciesFmt(q, 'Input', 'Abbrev', 'SIO.SWAL.v1')
%
% [eff, info] = dbGetEffort(q, 'Project', {'SOCAL', 'CINMS'}, 'SpeciesId', 'Bm', 'Call', {'A', 'B', 'D'}, 'Granularity', 'call')
% 
% eff will contian an Nx2 matrix of Matlab serial dates (datenum)
%
% info.deployments - Array of structs containing data sources
%   Each element contains either DeploymentId or EnsembleId
%   Example:  effInfo.deployments(3) is a struct containing
%   a DeploymentId:  SOCAL05-E
%
% info.effort_table - Contains a table of information about detection
%   effort that matched the query sorted by start date
%   Each row contains:
%       Start - start of effort (type datetime)
%       End - end of effort (type datetime)
%       Id - Detection document identifier (Id field)
%       RecordIdx - Integer that index into info.deployments to see 
%          where the data came from, or into data to see details
%          information about the detection effort
%
%         Start                    End                                          Id                                  RecordIdx
%  ____________________    ____________________    _________________________________________________________
%  13-Aug-2005 12:00:00    25-Aug-2005 19:20:00    'detections_GPL_v2_35_90_SOCAL01C_d01_050814_120000.d100'
%  25-Aug-2005 19:20:00    06-Sep-2005 02:40:00    'detections_GPL_v2_35_90_SOCAL01C_d05_050825_192000.d100'
%  06-Sep-2005 02:40:00    17-Sep-2005 10:00:00    'detections_GPL_v2_35_90_SOCAL01C_d09_050906_024000.d100'
%  ...
%
%  info.kinds_table - Table indicating details about what kind of
%    effort was conducted.  Each row contains:
%
%       SpeciesId - Attempted to detect this species
%       Group - Any additional information about the species, codes are
%          user defined and may be used to express hypotheses about
%          a species (e.g. some type of beaked whale that has not been
%          resolved to species, population group indicator, etc.
%       Call - Effort for a specific call type (may be absent)
%       Granularity - One of the following:
%          binned - Calls are reported over a fixed interval defined
%             by entry in column BinSize_min, which denotes the bin
%             duration in minutes.  (BinSize_min is NaN for other
%             granularities)
%          call - individual calls are reported
%          encounter - Calls are reported by the start and end of
%             a group of calls.
%       RecordIdx - Integer that index into info.deployments to see 
%          where the data came from, or into data to see details
%          information about the detection effort
%
% info.data - Structure array of information associated with the
%   effort. For example, the first row of the effort_table above
%   is from record 436.  When we access info(434).Detections, we see
%   that the record has the saem Id, Start, and End as the first row
%   (although Start/End are as Matlab serial dates here):
%            Id: {'detections_GPL_v2_35_90_SOCAL01C_d01_050814_120000.d100'}
%         Start: {[7.3254e+05]}
%           End: {[7.3255e+05]}
%    DataSource: [1×1 struct]
%          Kind: [1×1 struct]
% and drilling down into the Kind:  info(434).Detections.Kind
% we see that this specific effort for D calls.
%       SpeciesId: {'Balaenoptera musculus'}
%           Call: {'D'}
%    Granularity: {'call'}
%
%
% See also:  dbGetDetections

assert(dbVerifyQueryHandler(queryEngine), ...
    "First argument must be a query handler");

% Collecting benchmark statistics?
b_idx = find(strcmp(varargin(1:2:end), 'Benchmark'));
if ~ isempty(b_idx)
    bench_dir = varargin{(b_idx-1)*+2};
    varargin((b_idx-1)*2+[1 2]) = [];
    benchmark_p = true;
else
    bench_dir = [];
    benchmark_p = false;
end

% Values to be returned (complete paths)
if nargout > 1
    default_returns = [...
        "Detections/Id", ...
        "Detections/Effort/Start", ...
        "Detections/Effort/End", ...
        "Detections/DataSource", ...
        "Detections/Effort/Kind"];
else
    default_returns = [
        "Detections/Id", ...
        "Detections/Effort/Start", ...
        "Detections/Effort/End", ...
        "Detections/DataSource"];
end
    
r_idx = find(strcmp(varargin(1:2:end), 'return'));
if isempty(r_idx)
    % User did not specify a return statement, add one in
    varargin{end+1} = 'return';
    varargin{end+1} = default_returns;
else
    % if return is the i'th keyword, it is at varargin{2(i-1)+1}
    % and has an argument at varargin{2(i-1)+1+1} = varargin{2i}
    retvalues_idx = r_idx*2;
    retvals = varargin(retvalues_idx);
    % Return statements should all be strings/chars, convert to strings
    retvals = cellfun(@convertCharsToStrings, retvals, ...
        'UniformOutput', false);
    for idx = 1:length(retvals)
        varargin{retvalues_idx(idx)} = retvals{idx};
    end

    % Get full paths for any returns that user might have specified
    % and see if we need to add in the default ones.
    tmpmap = containers.Map();
    var_indices = [(r_idx-1)*2+1; r_idx*2];
    err = dbParseOptions(queryEngine, "Detections", ...
        tmpmap, "detections_effort", varargin{var_indices});
    
    existing_returns = tmpmap('return');
    add = strings(0,1);
    for d_idx = 1:length(default_returns)
        if ~any(strcmp(existing_returns, default_returns(d_idx)))
            add(end+1) = default_returns(d_idx);
        end
    end
    for add_idx = 1:length(add)
        varargin{end+1} = "return";
        varargin{end+1} = add(add_idx);
    end 
end
     
map = containers.Map();
map('enclose') = 1;  % Wrap elements around sets of loop values
map('namespaces') = 0;  % Strip namespaces from results

% Determine formats for species names in input/output
species_map = JSONSpeciesFmt(queryEngine, 'GetInput', 'GetOutput');
if ~ isempty(species_map)
    map("species") = species_map;
    tsnP = species_map.isKey('return');  % Note if we translate to strings
end
% Parse user arguments
err = dbParseOptions(queryEngine, "Detections", ...
    map, "detections_effort", varargin{:});
dbParseUnrecoverableErrorCheck(err);  % die if unrecoverable error
if ~ isempty(err)
    % User might have Deployments selection criteria.
    err = dbParseOptions(queryEngine, "Deployment", map, ...
        "deployments", err.unmatched{:});
    dbParseUnrecoverableErrorCheck(err);
    dbParseUnmatchedErrors(err);
end

json = jsonencode(map);

% Execute XQuery
query_timer = dbTimer();
xml_result = queryEngine.QueryJSON(json, 0);
query_elapsed = query_timer.elapsed();
    
parse_timer = dbTimer();

typemap={
    'idx','double';...
    'Deployment','double';...
    'Start','datetime';...
    'End','datetime';...
    'Score','double';...
    'BinSize_min','double';
    'Longitude', 'double';
    'Latitude', 'double';
    'Timestamp', 'datetime';
    'AudioTimestamp', 'datetime';
    'FrequencyMeasurements_Hz', 'double';
    };
if ~ tsnP
    typemap(end+1,:) = {'SpeciesId','double'};
end


xml_result = char(xml_result);
result=tinyxml2_tethys('parse',xml_result,typemap);

if iscell(result) && isempty(result{1})
    timestamps = zeros(0,2);  % empty
    info = [];
else
    % identify the parent of Detections (either result or
    % result.Detections)
    if isfield(result, "Record")
        % Joined documents from multiple collections are wrapped in Record
        detparent = result.Record;
    else
        detparent = result;
    end
    % Start/End under Return.Detections as Effort cannot appear more than once
    efforts = dbMergeStructures(detparent, 'Detections');

    tabular = table(...
        cell2mat([efforts.Start]'), cell2mat([efforts.End]'), ...
        string(vertcat(efforts.Id)), ...
        string(cellfun(@(x) get_field(x, 'DeploymentId'), {efforts.DataSource})'), ...
        string(cellfun(@(x) get_field(x, 'EnsembleId'), {efforts.DataSource})'), ...
        (1:length(efforts))', ...
        'VariableNames', {'Start', 'End', 'Id', 'DeploymentId', 'EnsembleId', 'RecordIdx'});

    % Sort data table if needed
    if ~ issorted(tabular.Start)
        [~, perm] = sort(tabular.Start);
        tabular = tabular(perm, :);
    end
    
    timestamps = [tabular.Start, tabular.End];
    
    % Reorder information associated with detections to be in same order
    if nargout > 1
        % deployments will contain information about the deployments
        % the detections were associated with (DeploymentId or
        % EnsembleName).  deploymentIdx tells us which deployment each
        % detection is associated with:
        % info.deployments(info.deploymentIdx(5)) tells us the identifier
        % for the fifth detection.
        detections = [detparent.Detections];
        % Convert start/end to datetime
        tabular.Start = datetime(tabular.Start, 'ConvertFrom', 'datenum');
        tabular.End = datetime(tabular.End, 'ConvertFrom', 'datenum');
        info.effort_table = tabular;
        info.deployments=[detections.DataSource];
        info.Id = string(arrayfun(@(x) x.Detections.Id, detparent));
        info.data = detparent;
        
        % Build a table showing effort for each kind
        % This code is a bit ugly as Matlab does not handle heterogeneous
        % substructures very well.
        
        % Populate the mandatory kind fields first, this will allow us 
        % to preallocate the optional ones
        species = arrayfun(@(x) [x.Kind.SpeciesId], detections, 'UniformOutput', false);
        species = string(horzcat(species{:})');
        granularity = arrayfun(@(x) [x.Kind.Granularity], detections, 'UniformOutput', false);
        granularity = string(horzcat(granularity{:})');
        
        % Add the RecordIdx and handle optional types
        % todo: make this data driven from schema
        binsize_min = nan(length(granularity), 1);
        subtypes = strings(length(granularity), 1);
        calls = strings(length(granularity), 1);
        record_idx = zeros(length(granularity), 1);
        group = strings(length(granularity), 1);
        freq_Hz = cell(length(granularity), 1);
        count = 1;
        for idx = 1:length(detections)
            for k=1:length(detections(idx).Kind)
                record_idx(count) = idx;  % track the effort group
                calls(count) = string(missing); 
                if isfield(detections(idx).Kind(k), 'Call') && ...
                    ~ isempty(detections(idx).Kind(k).Call)
                        calls(count) = detections(idx).Kind(k).Call{1};
                end
                subtypes(count) = string(missing);
                if isfield(detections(idx).Kind(k), 'Parameters')
                    if isfield(detections(idx).Kind(k).Parameters, 'Subtype') && ...
                            detections(idx).Kind(k).Parameters.Subtype{1} ~= ""
                        subtypes(count) = detections(idx).Kind(k).Parameters.Subtype{1};
                    end
                    if isfield(detections(idx).Kind(k).Parameters, 'FrequencyMeasurements_Hz')
                        freq_Hz{count} = detections(idx).Kind(k).Parameters.FrequencyMeasurements_Hz{1};
                    end
                end
                if isfield(detections(idx).Kind(k), 'SpeciesId_attr') && ...
                        isfield(detections(idx).Kind(k).SpeciesId_attr, 'Group')
                    group(count) = detections(idx).Kind(k).SpeciesId_attr.Group;
                else
                    group(count) = string(missing);
                end
                if strcmp(detections(idx).Kind(k).Granularity, 'binned')
                    binsize_min(count) = detections(idx).Kind(k).Granularity_attr.BinSize_min;
                end
                count = count + 1;
            end
        end
        
        % Build a table
        info.kinds_table = table(...
            record_idx, species, group, calls, ...
            granularity, binsize_min, ...
            'VariableNames', ...
               {'RecordIdx', 'SpeciesId', 'Group', ...
                'Call', 'Granularity', 'BinSize_min'});
        % Add in things we only want if they are present.
        if ~ all(cellfun(@isempty, subtypes))
            info.kinds_table.Subtype = subtypes;
        end
        if ~ all(cellfun(@isempty, freq_Hz))
            info.kinds_table.FrequencyMeasurements_Hz = freq_Hz;
        end            
    end
    parse_elapsed = parse_timer.elapsed();
end

if benchmark_p
    % Generate XQuery (set enclose=1, default)
    query = queryEngine.QueryJSON(json, 2, 1);
    dbWriteBench(bench_dir, query_elapsed, parse_elapsed, query, size(timestamps,1));
end
end

function value = get_field(s, f)
% One-off for pulling out string fields that may or may not be
% there
if isfield(s, f)
    value= s.(f);
else
    value = string(missing);
end
end

