function el_types = dbGetSchemaTypes(q, root_element)
% el_types = dbGetSchemaTypes(q, root_element)
% Determine element types from schema root element
% For internal use only

csv = q.getSchema(root_element);
csv_str = string(csv);

% Extract field names
csv_list = csv_str.splitlines();
fieldnames = csv_list(1).split(",");

% scan text, %q is possibly quoted text
hdrfmt = repmat("%q", length(fieldnames), 1);
hdrfmt = hdrfmt.join("");
data = textscan(csv_str, hdrfmt, 'Headerlines', 1, ...
    'Delimiter', ',');

% Convert to a table
str_arrays = cellfun(@string, data, 'UniformOutput', false);
schema = table(str_arrays{:}, 'VariableNames', fieldnames);

% Build up conversions
schema.element = regexprep(schema.path, ".*/", "");  % last ele in path
types = ["xs:double", "list(xs:double)", "xs:dateTime", "list(xs:dateTime)"];
convert_to = ["double", "double", "datetime", "datetime"];
el_types = strings(0,2);
for idx = 1:length(types)
    subtable = schema(strcmp(schema.type, types(idx)), :);
    N = height(subtable);
    if N > 0
        target_type = repmat(convert_to(idx), N, 1);
        el_types(end+1:end+N, :) = horzcat(subtable.element, target_type);
    end
end

% We may now have duplicates.  In general, the schemata do not have
% different types when the same name appears multiple times.
% Just take any one of them.
el_types = unique(el_types, 'rows');
% Convert to cell array expected by XML parsing function
el_types = cellstr(el_types);



