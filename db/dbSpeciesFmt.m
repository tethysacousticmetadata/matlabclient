function libfmt = dbSpeciesFmt(varargin)
% dbSpeciesFmt(QueryHandler, Type, Format, Option)
% Sets the species naming format used for XQueries (tsn, Latin name, 
% abbreviation) as well as how those results will be displayed.
%
% QueryHandler is a query handler object produced by dbInit.
% If provided and Fromat is set to Abbrev, the name of the abbreviation
% map will be verified.  
%
% Type is 'Input' or 'Output' representing XQueries or Xquery results
% respectively
% Format indicates how the values are specified or reported, and is one of the following:
%  'tsn' - ITIS tsn
%  'Latin' - ITIS completename (Latin species/family/order/... name)
%  'Vernacular', Language - ITIS vernacular.  Language must be one of the
%     the following:  'English', 'French', 'Portugese', 'Spanish'.
%     Vernacular is only complete for English and will cause problems
%     for some queries when using other languages
%  'Abbrev', SpeciesAbbreviationMap - Use custom abbreviations based
%     on the specified abbreviaiton map
%
% To retrieve the current format, call with Type set to GetInput or 
% GetOutput.

if isjava(varargin{1}) && strcmp('dbxml.Queries',class(varargin{1}))
    query_h = varargin{1};
    varargin(1) = [];
else
    error("Starting with Tethys 3.1, query handler is mandatory");
end

narginchk(1, Inf);
Type = varargin{1};
varargin(1) = [];


switch(Type)
    case 'Input'
        query_h.setSpeciesIdInput(varargin{:});
    case 'Output'
        query_h.setSpeciesIdOutput(varargin{:});
    case 'GetInput'
        libfmt = query_h.tsnEncode();
    case 'GetOutput'
        libfmt = query_h.tsnDecode();
    otherwise
        error('Bad Type');
end


